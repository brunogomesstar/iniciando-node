const express = require ('express');
const { listen } = require('express/lib/application');
const port = 8000;
let app = express();

app.get('/', (request, response)=>{
    response.send("Resultado sucesso RAÍZ");
    console.log("GET /");
});

app.get('/clientes', (request, response)=>{
    let obj = request.query;
    response.send("Resultado sucesso /CLIENTES " + obj.nome);
    console.log("GET /CLIENTES");
});

app.listen(port,() =>{
    console.log(`Projeto executando na porta ${port}`);
});
